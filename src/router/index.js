import Vue from 'vue'
import VueRouter from 'vue-router'
import MyJournals from '../views/MyJournals.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'MyJournals',
		component: MyJournals,
	},
	{
		path: '/add-journal',
		name: 'AddJournal',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () =>
			import(/* webpackChunkName: "about" */ '../views/AddJournal.vue'),
	},
	{
		path: '/:id',
		name: 'ReadJournal',
		component: () => import('../views/ReadJournal.vue'),
	},
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
})

export default router
