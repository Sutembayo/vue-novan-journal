import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		journals: [
			{
				id: 1589888492909,
				day: 'Selasa',
				date: '19 Mei 2020',
				content:
					'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis dicta explicabo praesentium iste natus veritatis tempore, at, repellat sapiente repellendus et vel. Quod consequuntur et molestiae eius nemo blanditiis quam.',
				mood: 'I feel like a shit',
			},
			{
				id: 1589888511536,
				day: 'Senin',
				date: '18 Mei 2020',
				content:
					'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis dicta explicabo praesentium iste natus veritatis tempore, at, repellat sapiente repellendus et vel. Quod consequuntur et molestiae eius nemo blanditiis quam.',
				mood: 'Good',
			},
		],
	},
	getters: {
		displayJournal: (state) => {
			return state.journals.map((journal) => {
				return {
					id: journal.id,
					day: journal.day,
					date: journal.date,
					content: journal.content.substring(0, 200),
					mood: journal.mood,
				}
			})
		},
	},
	mutations: {
		addJournal: (state, payload) => {
			state.journals = [payload, ...state.journals]
			// console.log(payload, state.journals)
		},
		sortJournalDescending: (state) => {
			state.journals.sort((a, b) => (a['id'] > b['id'] ? -1 : 1))
		},
		sortJournalAscending: (state) => {
			state.journals.sort((a, b) => (a['id'] < b['id'] ? -1 : 1))
		},
	},
	actions: {},
	modules: {},
})
